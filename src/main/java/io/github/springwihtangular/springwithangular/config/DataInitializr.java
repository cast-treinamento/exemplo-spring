package io.github.springwihtangular.springwithangular.config;


import io.github.springwihtangular.springwithangular.entity.Perfil;
import io.github.springwihtangular.springwithangular.repository.RoleRepository;
import io.github.springwihtangular.springwithangular.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {

        List<Perfil> perfis = roleRepository.findAll();

        if (perfis.isEmpty()) {

            Perfil perfil = new Perfil("Admin");
            Perfil perfil2 = new Perfil("Oreia");
            this.roleRepository.save(perfil);
            this.roleRepository.save(perfil2);

        }

    }


}

