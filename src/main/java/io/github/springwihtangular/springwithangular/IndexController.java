package io.github.springwihtangular.springwithangular;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @RequestMapping(name = "/", method = RequestMethod.GET)
    public String getIndex(){
        return "Olá esse é o meu primeiro projeto com o Spring Boot";
    }

}
