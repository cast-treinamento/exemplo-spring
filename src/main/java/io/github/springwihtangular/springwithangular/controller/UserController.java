package io.github.springwihtangular.springwithangular.controller;


import io.github.springwihtangular.springwithangular.entity.Usuario;
import io.github.springwihtangular.springwithangular.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Usuario> save(@RequestBody Usuario user){
        user = this.userRepository.save(user);
        return new ResponseEntity<Usuario>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Usuario>> list(){
        return new ResponseEntity<List<Usuario>>(userRepository.findAll(), HttpStatus.OK);
    }
}
