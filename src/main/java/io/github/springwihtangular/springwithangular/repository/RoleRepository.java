package io.github.springwihtangular.springwithangular.repository;

import io.github.springwihtangular.springwithangular.entity.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Perfil, Long> {

}
