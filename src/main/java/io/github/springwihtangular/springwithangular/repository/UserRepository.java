package io.github.springwihtangular.springwithangular.repository;

import io.github.springwihtangular.springwithangular.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Usuario, Long> {

}
