# Spring Boot

Spring Boot e Gitlab
---

* Baixar o Projeto via Spring Initializr
* Configurar o Intellij IDEA
* Run Server
* Clean install Maven
* Rodar o projeto
* IndexController
* @SpringBootApplication
* Criar um projeto no gitlab
* Subir o projeto na master
* Criar a branch develop
* Usar a nomenclatura do gitflow
* Gerar a tag do dia

Spring Data
---

* Especificação JPA
* Entidades
* Interfaces Spring Data Repository
* Spring Data na prática

MVC e Criação de APIs REST
---

* MVC
* Controller
* Criação de um endpoint
* Integração do endpoint com o Repository
* Postman


